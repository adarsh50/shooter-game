import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'
import { HYDRATE } from 'next-redux-wrapper'

// Define a service using a base URL and expected endpoints
export const shooterApi = createApi({
  reducerPath: 'shooterApi',
  baseQuery: fetchBaseQuery({ baseUrl: 'https://www.freetogame.com/api' }),
  tagTypes:["shooterApi"],
  extractRehydrationInfo(action, { reducerPath }) {
    if (action.type === HYDRATE) {
      return action.payload[reducerPath]
    }
  },
  endpoints: (builder) => ({
    getShooterByName: builder.query<object,void>({
      query: () => {
        return {
          url : "/games",
          method:"GET",
        }
      }
      // provideTags:["shooterApi"],
    }),
    getShooterById:builder.query<object,{id:number}>({
      query: ({id}) => {
        return {
          url : `/game?id=${id}`,
          method:"GET",
        }
      }
    })
  }),

})

export const { getShooterByName,getShooterById } = shooterApi.endpoints