import React from "react";
import { configureStore } from '@reduxjs/toolkit';
import {createWrapper} from "next-redux-wrapper";
import { shooterApi } from "./api/shooter";

export const makeStore = () =>{
return  configureStore({
  reducer: {
    [shooterApi.reducerPath]:shooterApi.reducer
  },
  middleware: (getDefaultMiddleware) =>
  getDefaultMiddleware().concat(shooterApi.middleware),
})
}

const store = makeStore();

export type AppStore = ReturnType<typeof makeStore>;
export type RootState = ReturnType<AppStore['getState']>;
export type AppDispatch = typeof store.dispatch;
export const wrapper = createWrapper(makeStore, { debug: true });

export default store
