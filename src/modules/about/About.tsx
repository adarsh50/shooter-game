import React from "react";
import { Typography, Box } from "@mui/material";

import AboutContainer, { AboutContainerWrapper, AboutText } from "./style";

const About = () => {
  return (
    <AboutContainer>
      <Box>
        <AboutContainerWrapper variant="h1">About Game</AboutContainerWrapper>
      </Box>
      <Box>
        <AboutText variant="h4">
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Maiores
          cupiditate odio nesciunt cumque itaque, commodi et excepturi tempora
          eos rerum inventore dolorum quae consequuntur porro, debitis eius
          repellat nihil! Assumenda illum ullam accusamus porro aspernatur,
          molestias debitis quaerat sapiente officiis omnis consequuntur
          corrupti temporibus soluta magnam voluptate voluptas veniam,
          perspiciatis dignissimos maxime quia. Veritatis odio rerum, animi
          mollitia voluptatibus unde rem, omnis sunt minima totam quo dolores id
          vitae deleniti cupiditate iusto quas molestiae reiciendis, asperiores
          corrupti doloremque libero ad consequuntur. Quos architecto excepturi
          nihil optio quasi dolorem dicta odit, cum animi eum minus eaque
          doloremque tempore, obcaecati eveniet corrupti nobis necessitatibus.
          Aut sed eveniet exercitationem impedit, ab autem culpa molestiae,
          ullam eius officiis quos fugiat debitis, placeat quod. Obcaecati ipsum
          dicta consequuntur voluptatum quisquam commodi id facere, vero harum
          tempora, fugit quidem voluptatibus, assumenda quia corporis.
          Distinctio recusandae nisi rem maxime commodi nostrum. Amet facilis,
          sit dolorum autem nisi totam placeat voluptas delectus odit labore,
          molestias debitis voluptate iste sint. Ducimus, soluta. Dicta dolorum
          similique ducimus nobis nisi asperiores cupiditate? Hic quisquam
          natus, fuga numquam sunt provident. Hic mollitia animi iste nisi quo,
          a dolorem necessitatibus illo, rerum, facilis laudantium quibusdam
          itaque earum! Beatae, nobis? Non repellendus at magnam distinctio
          eligendi! Molestias, ad nulla, beatae eaque ducimus, laudantium vitae
          incidunt qui facere eos praesentium reprehenderit. Dolore recusandae
          consequatur ratione, cum quos labore? Saepe illum esse obcaecati
          nesciunt doloremque quibusdam nam recusandae maxime corporis sed quam
          placeat, iure dolorum nisi quae totam enim soluta ut tenetur
          accusamus, omnis earum sapiente aliquid ad? Impedit facilis dolor
          blanditiis officiis consequatur totam tempore alias! Eum, animi?
          Tempora, explicabo esse id quia maxime magnam beatae odit nesciunt
          voluptate minus adipisci. Molestias, obcaecati doloribus quas
          accusamus odio soluta. Ea labore pariatur eligendi? Voluptate beatae
          numquam consequuntur distinctio impedit inventore, ipsum quibusdam
          officia. Quo, aspernatur quam ipsum cum laboriosam reiciendis modi
          excepturi molestias maxime nemo neque! Exercitationem, ratione.
          Asperiores eos incidunt, cum impedit odit dignissimos sunt eius iure
          quo reiciendis non dolorem esse ullam unde molestias natus id cumque
          nostrum quasi iste. Maxime nulla, doloribus obcaecati perspiciatis
          incidunt repudiandae. Placeat quod officiis suscipit ratione, expedita
          unde rem nihil perspiciatis amet tempore ut voluptatibus eum non.
          Fugiat, enim. Voluptatum expedita ad omnis veniam similique ducimus
          vitae commodi maiores sed eius iusto, officia facilis numquam eos
          necessitatibus recusandae obcaecati dignissimos dolores, inventore
          quos quas laborum minima. Sit cumque dolorem natus placeat deleniti
          voluptate, quibusdam rem dicta nulla, possimus labore? Enim libero
          aliquid harum ab quasi? Harum eos officiis est vel voluptates
          assumenda aut quibusdam tempore eaque consectetur pariatur itaque,
          similique numquam, enim fugit excepturi repellendus, tempora sed dolor
          perferendis. Id laborum veritatis minima porro sunt, et rem ea
          distinctio ab inventore dignissimos. Possimus natus perspiciatis
          excepturi? Sit deleniti laborum, eligendi veritatis reiciendis quas
          quam in nemo nobis quaerat impedit odio ipsa mollitia. Dolores sed
          exercitationem ducimus corrupti maxime aspernatur laudantium laborum
          fugit? Doloribus necessitatibus, quia eum ducimus fugit odit
          doloremque tempore error libero animi vero cupiditate dolorum possimus
          nam hic quaerat. Perspiciatis, rem reiciendis sit voluptates possimus
          at.
        </AboutText>
      </Box>
    </AboutContainer>
  );
};

export default About;
