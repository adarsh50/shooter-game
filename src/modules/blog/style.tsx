import { styled, Box, Typography } from "@mui/material";

const BlogPostContainer = styled(Box)({
  padding: "20px 40px 40px 40px",
});

export const BlogPostsWrapper = styled(Typography)({
  display: "flex",
  justifyContent: "center",
  fontSize: "72px",
  fontWeight: "500",
  paddingBottom: "40px",
  "@media (min-width:0px) and (max-width:768px)": {
    fontSize: "50px",
  },
});

export const BlogPostSection = styled(Typography)({
  fontSize: "18px",
  "@media (min-width:0px) and (max-width:768px)": {
    fontSize: "16px",
  },
});

export const BlogPostSectionWrapper = styled(Box)({
  marginTop: "20px",
});

export const BlogPostContain = styled(Typography)({
  fontSize: "18px",
  "@media (min-width:0px) and (max-width:768px)": {
    fontSize: "16px",
  },
});

export const BlogPostThirdSection = styled(Box)({
  marginTop: "20px",
});

export const BlogPostContainWrapper = styled(Typography)({
  fontSize: "18px",
  "@media (min-width:0px) and (max-width:768px)": {
    fontSize: "16px",
  },
});

export default BlogPostContainer;
