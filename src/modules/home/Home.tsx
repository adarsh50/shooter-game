import React from "react";
import shooter from "../../assests/images/battle_img.jpg";
import { Box, Typography } from "@mui/material";
import Image from "next/image";

import HomeContainer, {
  HomeActionGames,
  HomeContainerWrapper,
  HomeRealisticBattles,
} from "./style";

const Home = () => {
  return (
    <HomeContainer>
      <HomeContainerWrapper>
        <HomeActionGames variant="h3">Action Games</HomeActionGames>
        <HomeRealisticBattles variant="h2" sx={{}}>
          Realistic Battles
        </HomeRealisticBattles>
      </HomeContainerWrapper>
    </HomeContainer>
  );
};

export default Home;
