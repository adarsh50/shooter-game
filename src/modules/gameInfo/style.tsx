import { styled, Card, Typography } from "@mui/material";

const GameInfoContainer = styled("div")({
  margin: "50px",
  display: "flex",
  "@media (min-width:0px) and (max-width:768px)": {
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
  },
});

export const GameInfoCard = styled(Card)({
  width: "300px",
  height: "auto",
});

export const GameInfoTitle = styled(Typography)({
  fontSize: "24px",
  fontWeight: "700",
});

export const GameInfoAboutSection = styled("div")({
  marginLeft: "40px",
  "@media (min-width:0px) and (max-width:768px)": {
    margin: "0px",
  },
});

export const GameInfoTitleWrapper = styled(Typography)({
  fontSize: "40px",
  fontWeight: "600",
  "@media (min-width:0px) and (max-width:768px)": {
    fontSize: "30px",
    marginTop: "20px",
  },
});

export const GameInfoDescription = styled(Typography)({
  fontSize: "18px",
  paddingTop: "30px",
  "@media (min-width:0px) and (max-width:768px)": {
    fontSize: "16px",
  },
});

export default GameInfoContainer;
