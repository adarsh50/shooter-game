import React, { useState } from "react";
import {
  Box,
  List,
  ListItemText,
  ButtonGroup,
  Button,
  Drawer,
  Typography,
  ListItemButton,
  Divider,
  ListItem,
  IconButton,
} from "@mui/material";
import Image from "next/image";
import gameLogo from "../../assests/images/game_img.png";
import { SearchOutlined } from "@mui/icons-material";
import { FacebookOutlined } from "@mui/icons-material";
import Twitter from "@mui/icons-material";
import { YouTube } from "@mui/icons-material";
import { Google } from "@mui/icons-material";
import Instagram from "@mui/icons-material/Instagram";
import Link from "next/link";
import MenuIcon from "@mui/icons-material/Menu";

import GamePageHeader, {
  GamePageLogo,
  HeaderIcon,
  HeaderList,
  HeaderLoginButton,
  HeaderSignUpButton,
} from "./style";
import { Header } from "../../../interfaces/interfaces";

const Header = ({
  navItems,
  drawerWidth,
  mobileOpen,
  setMobileOpen,
  handleDrawerToggle,
  drawer,
}: Header) => {
  return (
    <header>
      <GamePageHeader>
        <GamePageLogo>
          <Image src={gameLogo} alt="gameLogo" height={50} width={80} />
        </GamePageLogo>
        <IconButton
          color="inherit"
          aria-label="open drawer"
          edge="start"
          onClick={handleDrawerToggle}
          sx={{ marginLeft: "5px", display: { sm: "none" } }}
        >
          <MenuIcon sx={{ color: "white" }} />
        </IconButton>
        <Box sx={{ display: { xs: "none", sm: "block" } }}>
          {/* <HeaderList>
          <Link href="/" style={{ textDecoration: "none" }}>
            <ListItemText sx={{ color: "white" }} primary="Home" />
          </Link>
          <Link href="/about" style={{ textDecoration: "none" }}>
            <ListItemText sx={{ color: "white" }} primary="About" />
          </Link>
          <Link href="/blog" style={{ textDecoration: "none" }}>
            <ListItemText sx={{ color: "white" }} primary="Blog" />
          </Link>
          <Link href="/games" style={{ textDecoration: "none" }}>
            <ListItemText sx={{ color: "white" }} primary="Games" />
          </Link>
          <ListItemText sx={{ color: "white" }} primary="Community" />
          <ListItemText sx={{ color: "white" }} primary="eSport" />
          <ListItemText sx={{ color: "white" }} primary="Pages" />
          <ListItemText sx={{ color: "white" }} primary="Contact" />
        </HeaderList> */}
          {navItems.map((item) => (
            <Button key={item.title} sx={{ color: "#fff" }}>
              <Link href={item.link} style={{ textDecoration: "none" }}>
                <ListItemText sx={{ color: "white" }} primary={item.title} />
              </Link>
            </Button>
          ))}
        </Box>
        <HeaderIcon>
          <IconButton>
            <SearchOutlined sx={{ color: "white" }} />
          </IconButton>
          <IconButton>
            <FacebookOutlined sx={{ color: "white" }} />
          </IconButton>
          {/* <TwitterIcon sx={{ color: "white" }} /> */}
          <IconButton>
            <YouTube sx={{ color: "white" }} />
          </IconButton>
          <IconButton>
            <Google sx={{ color: "white" }} />
          </IconButton>

          {/* <InstagramIcon sx={{ color: "white" }} /> */}
        </HeaderIcon>

        <Box>
          <ButtonGroup
            variant="contained"
            aria-label="outlined primary button group"
          >
            <HeaderLoginButton>Login</HeaderLoginButton>
            <HeaderSignUpButton>SignUp</HeaderSignUpButton>
          </ButtonGroup>
        </Box>
        <Box component="nav">
          <Drawer
            // container={container}
            variant="temporary"
            open={mobileOpen}
            onClose={handleDrawerToggle}
            ModalProps={{
              keepMounted: true, // Better open performance on mobile.
            }}
            sx={{
              display: { xs: "block", sm: "none" },
              "& .MuiDrawer-paper": {
                boxSizing: "border-box",
                width: drawerWidth,
                justifyContent: "center",
                alignItems: "center",
              },
            }}
          >
            {drawer}
          </Drawer>
        </Box>
      </GamePageHeader>
    </header>
  );
};

export default Header;
