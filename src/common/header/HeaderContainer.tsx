import React, { useState } from "react";
import Header from "./Header";
import {
  Box,
  Typography,
  Divider,
  List,
  ListItem,
  ListItemButton,
  ListItemText,
} from "@mui/material";
import Link from "next/link";

const HeaderContainer = () => {
  const drawerWidth = 300;
  // const navItems = [
  //   "Home",
  //   "About",
  //   "Blog",
  //   "Games",
  //   "Community",
  //   "ESports",
  //   "Pages",
  //   "Contact",
  // ];

  const navItems = [
    {
      title: "Home",
      link: "/home",
    },
    {
      title: "About",
      link: "/about",
    },
    {
      title: "Blog",
      link: "/blog",
    },
    {
      title: "Games",
      link: "/games",
    },
    // {
    //   title: "Community",
    //   link: "/community",
    // },
    // {
    //   title: "ESports",
    //   link: "/eSports",
    // },
    {
      title: "Pages",
      link: "/pages",
    },
    {
      title: "Contact",
      link: "/contact",
    },
  ];

  const [mobileOpen, setMobileOpen] = useState(false);

  const handleDrawerToggle = () => {
    setMobileOpen((prevState) => !prevState);
  };

  const drawer = (
    <Box onClick={handleDrawerToggle} sx={{ textAlign: "center" }}>
      <List>
        {navItems.map((item) => (
          <ListItem key={item.title} disablePadding>
            <ListItemButton sx={{ textAlign: "center" }}>
              <Link href={item.link} style={{ textDecoration: "none" }}>
                <ListItemText
                  sx={{ color: "black", textAlign: "center" }}
                  primary={item.title}
                />
              </Link>
            </ListItemButton>
          </ListItem>
        ))}
      </List>
    </Box>
  );

  return (
    <Header
      navItems={navItems}
      drawerWidth={drawerWidth}
      mobileOpen={mobileOpen}
      setMobileOpen={setMobileOpen}
      handleDrawerToggle={handleDrawerToggle}
      drawer={drawer}
    />
  );
};

export default HeaderContainer;
