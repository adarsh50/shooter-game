import { ReactNode } from "react";

export interface LayoutProps {
  children: ReactNode;
}

export interface navItems {
  title: string;
  link: string;
}

export interface NavContents {
  navItems: navItems[];
}

export interface Header {
  navItems: navItems[];
  mobileOpen: boolean;
  drawerWidth: number;
  setMobileOpen: React.Dispatch<React.SetStateAction<boolean>>;
  handleDrawerToggle: () => void;
  drawer: JSX.Element;
}

export interface props {
  data: Game[];
}

export interface Game {
  id: number;
  title: string;
  thumbnail: string;
  short_description: string;
  game_url: string;
  genre: string;
  platform: string;
  publisher: string;
  developer: string;
  release_date: string;
  freetogame_profile_url: string;
}

export interface ShooterProps {
  data: Shooter;
}

export interface Shooter {
  id: number;
  title: string;
  thumbnail: string;
  developer: string;
  description: string;
  short_description: string;
}
