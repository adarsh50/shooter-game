import React from "react";
import BlogContainer from "../../src/modules/blog/BlogContainer";
import Head from "next/head";

const Blog = () => (
  <>
    <Head>
      <title>Blog post</title>
      <meta name="description" content="next js blog post" />
    </Head>
    <BlogContainer />
  </>
);

export default Blog;
