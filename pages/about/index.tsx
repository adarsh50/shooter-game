import React from "react";
import AboutContainer from "../../src/modules/about/AboutContainer";
import Head from "next/head";

const About = () => (
  <>
    <Head>
      <title>About Page</title>
      <meta name="description" content="next js about Page" />
    </Head>
    <AboutContainer />
  </>
);

export default About;
