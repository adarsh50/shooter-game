import Head from "next/head";

import HomeContainer from "../src/modules/home/HomeContainer";

const Home = () => (
  <>
    <Head>
      <title>Home</title>
      <meta name="description" content="next js home page" />
    </Head>
    <HomeContainer />
  </>
);

export default Home;
