import React from "react";
import { makeStore } from "../../store/configureStore";
import Head from "next/head";

import { getShooterByName } from "../../store/api/shooter";
import { props } from "../../interfaces/interfaces";
import GameContainer from "../../src/modules/games/GameContainer";

const ShooterGames = ({ data }: props) => (
  <>
    <Head>
      <title>Shooter Games</title>
      <meta name="description" content="next js shooter games" />
    </Head>
    <GameContainer data={data} />
  </>
);

export default ShooterGames;

export async function getServerSideProps() {
  const store = makeStore();
  const data = await store.dispatch(getShooterByName.initiate());

  // Pass data to the page via props
  return { props: data };
}
