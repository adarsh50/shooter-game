import * as React from "react";
import Head from "next/head";
import { AppProps } from "next/app";
import { ThemeProvider } from "@mui/material/styles";
import CssBaseline from "@mui/material/CssBaseline";
import { CacheProvider, EmotionCache } from "@emotion/react";
import theme from "../src/mui_imports/theme";
import createEmotionCache from "../src/mui_imports/createEmotionCache";
import { Provider } from "react-redux";

import LayoutContainer from "../src/common/layout/LayoutContainer";
import store from "../store/configureStore";

// Client-side cache, shared for the whole session of the user in the browser.
const clientSideEmotionCache = createEmotionCache();

interface MyAppProps extends AppProps {
  emotionCache?: EmotionCache;
}

export default function MyApp(props: MyAppProps) {
  const { Component, emotionCache = clientSideEmotionCache, pageProps } = props;
  return (
    <CacheProvider value={emotionCache}>
      <Head>
        <title>games</title>
        <meta name="viewport" content="initial-scale=1,width=device-width" />
      </Head>
      <ThemeProvider theme={theme}>
        {/* CssBaseline kickstart an elegant, consistent, and simple baseline to build upon. */}
        <CssBaseline />
        <Provider store={store}>
          <LayoutContainer>
            <Component {...pageProps} />
          </LayoutContainer>
        </Provider>
      </ThemeProvider>
    </CacheProvider>
  );
}
