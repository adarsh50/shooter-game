import React from "react";
import Head from "next/head";

import HomeContainer from "../../src/modules/home/HomeContainer";

const Home = () => {
  return (
    <>
      <Head>
        <title>Home</title>
      </Head>
      <HomeContainer />
    </>
  );
};

export default Home;
